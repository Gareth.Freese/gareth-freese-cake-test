package com.example.garethfreesecaketest.models.network

data class CakeNetworkModel(
    val title: String,
    val desc: String,
    val image: String
)