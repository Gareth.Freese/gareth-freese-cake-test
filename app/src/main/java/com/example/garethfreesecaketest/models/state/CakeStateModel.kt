package com.example.garethfreesecaketest.models.state

data class CakeStateModel(
    val title: String,
    val description: String,
    val image: String
)