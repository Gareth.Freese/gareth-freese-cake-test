package com.example.garethfreesecaketest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.garethfreesecaketest.adapter.CakeAdapter
import com.example.garethfreesecaketest.databinding.ActivityMainBinding
import com.example.garethfreesecaketest.models.state.CakeStateModel
import com.example.garethfreesecaketest.models.state.ViewState
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

// Todo - move to string resources
private const val ERROR_MESSAGE = "Oops something went wrong! Please try again"
private const val RETRY_TEXT = "Retry"

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MainViewModel

    private lateinit var binding: ActivityMainBinding

    private lateinit var adapter: CakeAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        injectDependencies()

        initAdapter()
        initObservers()
        initRefreshListener()
        viewModel.getCakes()
    }

    private fun injectDependencies() {
        (applicationContext as CakeTestApplication).appComponent.inject(this)
        viewModel = ViewModelProvider(this, viewModelFactory)[MainViewModel::class.java]
    }

    private fun initObservers() {
        viewModel.cakeViewState.observe(this, ::handleCakeViewStateChange)
    }

    private fun initAdapter() {
        adapter = CakeAdapter(::cakeItemClickListener)
        binding.cakeRecyclerView.adapter = adapter
        binding.cakeRecyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun initRefreshListener() {
        with(binding.refreshCakes) {
            setOnRefreshListener {
                isRefreshing = false
                viewModel.getCakes()
            }
        }
    }

    private fun handleCakeViewStateChange(cakeViewState: ViewState<List<CakeStateModel>, Exception>) {
        when (cakeViewState) {
            is ViewState.Loading -> toggleLoadingState(true)
            is ViewState.Error -> showErrorSnackbar(cakeViewState.error)
            is ViewState.Success -> showCakeData(cakeViewState.result)
        }
    }

    private fun toggleLoadingState(isLoading: Boolean) {
        when (isLoading) {
            true -> binding.progressIndicator.visibility = View.VISIBLE
            false -> binding.progressIndicator.visibility = View.GONE
        }
    }

    // Todo - With more time I would have created a custom Exception class and tailored the message depending on the type of error
    private fun showErrorSnackbar(exception: Exception) {
        toggleLoadingState(false)
        Snackbar.make(
            binding.mainConstraintLayout,
            ERROR_MESSAGE,
            Snackbar.LENGTH_INDEFINITE
        ).setAction(RETRY_TEXT) {
            viewModel.getCakes()
        }.show()
    }

    private fun showCakeData(cakes: List<CakeStateModel>) {
        toggleLoadingState(false)
        adapter.updateCakeData(cakes)
    }

    // Todo - With more time I would have implemented a fragment popup/overlay and Jetpack navigation
    private fun cakeItemClickListener(description: String) {
        Toast.makeText(
            this,
            description,
            Toast.LENGTH_LONG
        ).show()
    }

}