package com.example.garethfreesecaketest.retrofit

import com.example.garethfreesecaketest.models.network.CakeNetworkModel
import retrofit2.http.GET

interface CakeApi {

    @GET("waracle_cake-android-client")
    suspend fun getCakes(): List<CakeNetworkModel>
}