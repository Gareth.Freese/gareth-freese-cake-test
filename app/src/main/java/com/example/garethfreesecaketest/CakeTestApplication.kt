package com.example.garethfreesecaketest

import android.app.Application
import com.example.garethfreesecaketest.dagger.AppComponent
import com.example.garethfreesecaketest.dagger.DaggerAppComponent

class CakeTestApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        appComponent = initDagger()
    }

    lateinit var appComponent: AppComponent

    private fun initDagger(): AppComponent =
        DaggerAppComponent.builder()
            .build()
}