package com.example.garethfreesecaketest.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.garethfreesecaketest.R
import com.example.garethfreesecaketest.databinding.ItemCakeBinding
import com.example.garethfreesecaketest.models.state.CakeStateModel

class CakeAdapter constructor(
    private val cakeClickListener: (String) -> Unit
) : RecyclerView.Adapter<CakeAdapter.CakeViewHolder>() {

    private val cakeList = arrayListOf<CakeStateModel>()

    fun updateCakeData(cakes: List<CakeStateModel>) {
        cakeList.clear()
        cakeList.addAll(cakes)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CakeViewHolder(
            ItemCakeBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: CakeViewHolder, position: Int) {
        holder.bind(cakeList[position])
    }

    override fun getItemCount() = cakeList.size

    inner class CakeViewHolder(private val binding: ItemCakeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        // Todo I would have liked to improve the ui with a little more time
            fun bind(cakeStateModel: CakeStateModel) {
                binding.cakeTitle.text = cakeStateModel.title

                Glide.with(binding.cakeImage)
                    .load(cakeStateModel.image)
                    .placeholder(R.drawable.cake)
                    .into(binding.cakeImage)

                binding.cakeListItem.setOnClickListener {
                    cakeClickListener(cakeStateModel.description)
                }
            }
    }
}