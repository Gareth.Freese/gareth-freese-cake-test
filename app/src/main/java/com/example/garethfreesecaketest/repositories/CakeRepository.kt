package com.example.garethfreesecaketest.repositories

import com.example.garethfreesecaketest.models.network.CakeNetworkModel

interface CakeRepository {

    suspend fun getCakes(): List<CakeNetworkModel>
}