package com.example.garethfreesecaketest.repositories

import com.example.garethfreesecaketest.models.network.CakeNetworkModel
import com.example.garethfreesecaketest.retrofit.CakeApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import javax.inject.Inject

class CakeRepositoryImpl @Inject constructor(
    retrofit: Retrofit
) : CakeRepository {

    private val cakeApi = retrofit.create(CakeApi::class.java)

    override suspend fun getCakes(): List<CakeNetworkModel> =
        withContext(Dispatchers.IO) {
            try {
                cakeApi.getCakes().ifEmpty {
                    throw Exception()
                }
            } catch (exception: Exception) {
                // todo - with more time I would have made a custom exception class and mapper for bespoke messages depending on the error code that would be used later on in the ui
                throw exception
            }
        }
}