package com.example.garethfreesecaketest.mappers

import com.example.garethfreesecaketest.models.network.CakeNetworkModel
import com.example.garethfreesecaketest.models.state.CakeStateModel
import javax.inject.Inject

class CakeStateModelMapper @Inject constructor() :
        (List<CakeNetworkModel>) -> List<CakeStateModel> {

    override fun invoke(cakeNetworkData: List<CakeNetworkModel>) =
        cakeNetworkData.distinct().map {
            CakeStateModel(
                title = it.title,
                description = it.desc,
                image = it.image
            )
        }.sortedBy { it.title }
}