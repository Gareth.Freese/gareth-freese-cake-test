package com.example.garethfreesecaketest.dagger

import com.example.garethfreesecaketest.repositories.CakeRepository
import com.example.garethfreesecaketest.repositories.CakeRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {

    @Binds
    abstract fun bindCakeRepository(repository: CakeRepositoryImpl): CakeRepository
}