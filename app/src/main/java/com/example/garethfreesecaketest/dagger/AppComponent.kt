package com.example.garethfreesecaketest.dagger

import com.example.garethfreesecaketest.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, ViewModelModule::class, RepositoryModule::class])
interface AppComponent {

    fun inject(activity: MainActivity)
}