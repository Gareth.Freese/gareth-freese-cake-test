package com.example.garethfreesecaketest

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.garethfreesecaketest.mappers.CakeStateModelMapper
import com.example.garethfreesecaketest.models.state.CakeStateModel
import com.example.garethfreesecaketest.models.state.ViewState
import com.example.garethfreesecaketest.repositories.CakeRepository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val cakeStateModelMapper: CakeStateModelMapper,
    private val cakeRepository: CakeRepository
) : ViewModel() {

    private val _cakeViewState = MutableLiveData<ViewState<List<CakeStateModel>, Exception>>()
    val cakeViewState: LiveData<ViewState<List<CakeStateModel>, Exception>> = _cakeViewState

    fun getCakes() {
        _cakeViewState.value = ViewState.Loading
        viewModelScope.launch(emitErrorState()) {
            cakeRepository.getCakes().let {
                _cakeViewState.value = ViewState.Success(cakeStateModelMapper(it))
            }
        }
    }

    private fun emitErrorState() =
        CoroutineExceptionHandler { _, throwable ->
            _cakeViewState.value = ViewState.Error(throwable as Exception)
        }
}