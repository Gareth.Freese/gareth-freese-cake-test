package com.example.garethfreesecaketest.repositories

import com.example.garethfreesecaketest.INVOKE_ONCE
import com.example.garethfreesecaketest.models.network.CakeNetworkModel
import com.example.garethfreesecaketest.retrofit.CakeApi
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThrows
import org.junit.Assert.assertTrue
import org.junit.Test
import retrofit2.Retrofit

class CakeRepositoryImplTest {

    private val cakeNetworkModel = mockk<CakeNetworkModel>()
    private val retrofitService = mockk<Retrofit> {
        coEvery { create(CakeApi::class.java).getCakes() } returns listOf(cakeNetworkModel)
    }
    private val cakeRepositoryImpl = CakeRepositoryImpl(retrofitService)

    @Test
    fun `given getCakes called then retrofit service should be invoked`() {
        runBlocking {
            cakeRepositoryImpl.getCakes()
        }

        coVerify(exactly = INVOKE_ONCE) { retrofitService.create(CakeApi::class.java).getCakes() }
    }

    @Test
    fun `given network response is a success and data is valid then should return network model`() {
        var cakes: List<CakeNetworkModel>?

        runBlocking {
            cakes = cakeRepositoryImpl.getCakes()
        }

        assertEquals(1, cakes?.size)
        assertTrue(cakes?.get(0) is CakeNetworkModel)
    }

    @Test(expected = Exception::class)
    fun `given network response is success but empty then should throw exception`() {
        coEvery { retrofitService.create(CakeApi::class.java).getCakes() } returns listOf()

        runBlocking {
            cakeRepositoryImpl.getCakes()
        }
    }

    @Test(expected = Exception::class)
    fun `given network response fails then should throw exception`() {
        coEvery { retrofitService.create(CakeApi::class.java).getCakes() } throws Exception()

        runBlocking {
            cakeRepositoryImpl.getCakes()
        }
    }
}