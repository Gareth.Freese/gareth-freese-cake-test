package com.example.garethfreesecaketest.mappers

import com.example.garethfreesecaketest.models.network.CakeNetworkModel
import org.junit.Assert.assertEquals
import org.junit.Test

private const val CARROT_CAKE_TITLE = "carrot cake title"
private const val BANANA_CAKE_TITLE = "banana cake title"
private const val CAKE_DESCRIPTION = "cake description"
private const val CAKE_IMAGE = "cake image"

class CakeStateModelMapperTest {

    private val mapper = CakeStateModelMapper()

    @Test
    fun `given mapper invoke then data should be mapped correctly`() {
        val cakeList = mapper(listOf(getCakeNetworkModel(CARROT_CAKE_TITLE)))

        assertEquals(1, cakeList.size)
        assertEquals(CARROT_CAKE_TITLE, cakeList[0].title)
        assertEquals(CAKE_DESCRIPTION, cakeList[0].description)
        assertEquals(CAKE_IMAGE, cakeList[0].image)
    }

    @Test
    fun `given mapper invoked with multiple cakes should order list correctly`() {
        val cakeList = mapper(
            listOf(
                getCakeNetworkModel(CARROT_CAKE_TITLE),
                getCakeNetworkModel(BANANA_CAKE_TITLE)
            )
        )

        assertEquals(2, cakeList.size)
        assertEquals(BANANA_CAKE_TITLE, cakeList[0].title)
        assertEquals(CARROT_CAKE_TITLE, cakeList[1].title)
    }

    @Test
    fun `given mapper invoked with duplicate entries should remove duplicates`() {
        val cakeList = mapper(
            listOf(
                getCakeNetworkModel(CARROT_CAKE_TITLE),
                getCakeNetworkModel(BANANA_CAKE_TITLE),
                getCakeNetworkModel(CARROT_CAKE_TITLE),
                getCakeNetworkModel(BANANA_CAKE_TITLE)
            )
        )

        assertEquals(2, cakeList.size)
    }

    private fun getCakeNetworkModel(cakeTitle: String) =
        CakeNetworkModel(
            title = cakeTitle,
            desc = CAKE_DESCRIPTION,
            image = CAKE_IMAGE
        )
}