package com.example.garethfreesecaketest

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.garethfreesecaketest.mappers.CakeStateModelMapper
import com.example.garethfreesecaketest.models.network.CakeNetworkModel
import com.example.garethfreesecaketest.models.state.CakeStateModel
import com.example.garethfreesecaketest.models.state.ViewState
import com.example.garethfreesecaketest.repositories.CakeRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MainViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @OptIn(DelicateCoroutinesApi::class)
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private val cakeNetworkModel = mockk<CakeNetworkModel>()
    private val cakeStateModel = mockk<CakeStateModel>()
    private val cakeStateModelMapper = mockk<CakeStateModelMapper> {
        every { this@mockk.invoke(listOf(cakeNetworkModel)) } returns listOf(cakeStateModel)
    }
    private val cakeRepository = mockk<CakeRepository> {
        coEvery { getCakes() } returns listOf(cakeNetworkModel)
    }

    private val cakeViewStateObserver = mockk<Observer<ViewState<List<CakeStateModel>, Exception>>>(relaxed = true)

    private val viewModel = MainViewModel(cakeStateModelMapper, cakeRepository)

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setup() {
        viewModel.cakeViewState.observeForever(cakeViewStateObserver)
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `given getCakes called should emit loading state`() {
        viewModel.getCakes()

        verify(exactly = INVOKE_ONCE) { cakeViewStateObserver.onChanged(ViewState.Loading) }
    }

    @Test
    fun `given getCakes called should invoke cake repository`() {
        viewModel.getCakes()

        coVerify(exactly = INVOKE_ONCE) { cakeRepository.getCakes() }
    }

    @Test
    fun `given getCakes called and network request is a success should invoke CakeStateModelMapper`() {
        viewModel.getCakes()

        coVerify(exactly = INVOKE_ONCE) { cakeStateModelMapper(listOf(cakeNetworkModel)) }
    }

    @Test
    fun `given getCakes called and network request is a success should emit success state`() {
        viewModel.getCakes()

        verify(exactly = INVOKE_ONCE) { cakeViewStateObserver.onChanged(ViewState.Success(listOf(cakeStateModel))) }
    }

    @Test
    fun `given getCakes called but network request fails should emit error state`() {
        val exception = mockk<Exception>()
        coEvery { cakeRepository.getCakes() } throws exception

        viewModel.getCakes()

        verify(exactly = INVOKE_ONCE) { cakeViewStateObserver.onChanged(ViewState.Error(exception)) }
    }
}